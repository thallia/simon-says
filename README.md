# Simon

This was a class project I did during fall semester of my sophomore year.

The semester was a class on digital logic design, where we covered the fundamentals of logic gates, circuits, and verilog.

Throughout the course we developed all of these modules alongside the Basys3 board from Digilent, with the last 4 weeks of lab specifically dedicated to `simon.v`, tying it all together.

**Video Doc**: https://youtu.be/Q7wbqPaH5FU
**Portfolio Post**: https://thalliatree.net/portfolio/simon-fpga.html
