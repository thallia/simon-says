module speaker(output reg speaker, input [2:0] thing, input clk, input SE);

reg [17:0] counter;
reg [17:0] tone;
always @* begin
case(thing)
0: tone = 227272;
1: tone = 202478;
2: tone = 180384;
3: tone = 170262;
4: tone = 151686;
5: tone = 135136;
6: tone = 120392;
7: tone = 113636;
endcase
end

initial begin
counter <= tone;
end

always @(posedge clk) begin
    counter <= counter - 1;
    if (counter == 0) begin
        counter <= tone;
    end
end

always @* begin
    speaker = 0;
    if ((counter < tone/2)) begin
        speaker = SE;
    end
end
endmodule