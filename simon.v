module top(output [2:0] simon_led0, simon_led1, simon_led2, simon_led3, output [3:0] an_n, input clk, btnC, btnU, input [3:0] simon_buttons_n,
           output [7:0] lcd_data, seg_n, output lcd_regsel, lcd_enable, speaker, input [1:0] sw);
           
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ VARIABLES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // buttons
    wire [3:0] simon_buttons = ~simon_buttons_n; // don't like active low
    wire p0, p1, p2, p3, p4, r0, r1, r2, r3, r4, h0, h1, h2, h3, h4; // pressed, released, and held wires for the debounce outputs
    wire reset = btnC; // reset
    
    // LCD screen
    wire lcd_available = 1;
    reg lcd_print;
    reg [8*16-1:0] topline, bottomline; // LCD display 
    reg [28:0] lcd_timer;
    reg [26:0] timer; // actual timer
    reg timerReset;
    reg [26:0] timerResetVal;
    reg timerEn;
    reg [26:0] spkrTimer;
    
    // random number generator
    reg [2:0] color; // for the random number generator, it'll generate the next sequence
    
    // state variables
    reg [31:0] state, nextState; 
    localparam RED = 001, GREEN = 010, BLUE = 100, YELLOW = 011, oneSecond = 100000000;
    localparam welcome = 0, waitRed = 1, simonPrint = 2, simonPlay = 3, userPlay = 4, playRed = 5, playGreen = 6, playBlue = 7, playYellow = 8, waitForPress = 9, 
        randomize = 10, simonRest = 11, simonCmp = 12, playerInit = 13, playerWaitForPress = 14, playerWaitForRelease = 15, playerCheck = 16, wincrement = 17, losecrement = 18,
        loseTone = 19, winTone = 20, playAgain = 21, winTone1 = 22, winTone2 = 23, loseTone1 = 24, loseTone2 = 25, cheatRepeat = 24;
    
    // scoring
    reg [31:0] score; // counter
    reg scoreReset;
    reg scoreEn;
    wire [3:0] ONES, TENS;
    reg [2:0] tone;
    reg random, step, speakerEn, ledEn;
    wire [1:0] randomOutput;
    reg rerun;

    //Iteration
    reg [31:0] simonCount; // iteration variable
    reg simonCountReset, simonCountEnable;
    
    // saving buttons
    reg [1:0] decodedButton, buttonSave;
    
    // speaker counter stuff
    reg spkrCounterReset, spkrCountEn;
    reg [31:0] spkrCount;
    
    // cheat mode vars
    wire [5:0] cheatRandom;
    reg [1:0] D, C, B, A;


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INSTANTIATIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    led_ctrl simon_colors (.led0(simon_led0), .led1(simon_led1), .led2(simon_led2), .led3(simon_led3), .clk(clk), .color(color), .enable(ledEn));
    
    lcd_string lcd_printer (.lcd_regsel(lcd_regsel), .lcd_enable(lcd_enable), .lcd_data(lcd_data), 
                  .available(lcd_available), .print(lcd_print), .topline(topline), .bottomline(bottomline), .reset(reset), .clk(clk) );
                  
    debouncer d1 (.pressed(p0), .released(r0), .held(h0), .button(simon_buttons[0]), .clk(clk), .reset(reset));
    debouncer d2 (.pressed(p1), .released(r1), .held(h1), .button(simon_buttons[1]), .clk(clk), .reset(reset));
    debouncer d3 (.pressed(p2), .released(r2), .held(h2), .button(simon_buttons[2]), .clk(clk), .reset(reset));
    debouncer d4 (.pressed(p3), .released(r3), .held(h3), .button(simon_buttons[3]), .clk(clk), .reset(reset));
    debouncer d5 (.pressed(p4), .released(r4), .held(h4), .button(btnU), .clk(clk), .reset(reset));
    
    PRNG pseudo_random (.cheatRandom(cheatRandom), .random(randomOutput), .rerun(rerun), .step(step), .randomize(random), .clk(clk), .reset(reset));
    
    binary_to_BCD score_coverter (.A(score), .ONES(ONES), .TENS(TENS));
    
    speaker s1 (.speaker(speaker), .thing(tone), .clk(clk), .SE(speakerEn));
    
    seg_ctrl seven_seg (.seg_n(seg_n), .an_n(an_n), .D(D), .C(C), .B(B), .A(A), .clk(clk));
    

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TIMERS & CLOCKS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// State Controller
always @(posedge clk) begin
    if(reset)
        state <= welcome; // initial state
    else
        state <= nextState;
end


// LCD Timer
always @(posedge clk) begin
    if (lcd_available) begin
        lcd_timer <= lcd_timer + 1;
    end
    if (lcd_timer >= 400000000) begin
        lcd_timer <= 0;
    end
end

// actual timer
always @(posedge clk) begin
    if (timerReset)
        timer <= timerResetVal;
    else if (timerEn)
        timer <= timer - 1;
end

// score keeper
always @(posedge clk) begin
    if(scoreReset)
        score <= 0;
    else if (scoreEn)
        score <= score + 1;
end

// iterator counter
always @(posedge clk) begin
    if (simonCountReset)
        simonCount <= 0;
    else if (simonCountEnable)
        simonCount <= simonCount + 1;
end

// win/lose tone counter
always @(posedge clk) begin
    if (spkrCounterReset)
        spkrCount <= 0;
    else if (spkrCountEn)
        spkrCount <= spkrCount + 1;

end

// button decoder
always @* begin
//decodedButton = 0;
    if (simon_buttons[0])
        decodedButton = 0;
    else if (simon_buttons[1])
        decodedButton = 1;
    else if (simon_buttons[2])
        decodedButton = 2;
    else if (simon_buttons[3])
        decodedButton = 3;
end


// cheat mode block
always @* begin
        D = 0;
        C = 0;
        B = 0;
        A = 0;
    if (sw[0]) begin
        C = cheatRandom[1:0];
        B = cheatRandom[3:2];
        A = cheatRandom[5:4];
        D = randomOutput;
    end
    
end


// ********************************************STATE MACHINE******************************************
always @* begin
    nextState = state;
    lcd_print = 0;
    scoreReset = 0;
    speakerEn = 0;
    scoreEn = 0;
    timerResetVal = 0;
    timerReset = 0;
    timerEn = 0;
    random = 0;
    step = 0;
    simonCountReset = 0;
    simonCountEnable = 0;
    rerun = 0;
    ledEn = 0;
    spkrCountEn = 0;
    spkrCounterReset = 0;
    
    
    case(state)
        welcome: begin // Initital welcome state
            if (lcd_available) begin
                lcd_print = 1;
                topline  =   "  SPICY SIMON!  ";
                bottomline = "    PRESS RED   "; 
            
//            if (lcd_timer >= 200000000) begin
//                topline    = " PRESS RED      ";
//                bottomline = "   TO BEGIN!    ";
//            end
            end
            if (p1) begin
                scoreReset = 1;
                nextState = randomize;
                end
            end
            
        randomize: begin
            random = 1;
            if (r1) begin // released red
                timerResetVal = oneSecond;
                timerReset = 1;
                nextState = simonPrint;
            end
            end    
            
        simonPrint: begin
            if (lcd_available) begin
                lcd_print = 1;
                topline  =   "LISTEN AND COPY!";
                bottomline = {"SCORE: ", 4'b0011, TENS, 4'b0011, ONES, "       "};
                timerEn = 1;
                end
            if(timer == 0) begin
                //nextState = simonPlay;
                simonCountReset = 1;
                timerResetVal = 0.75*oneSecond;
                timerReset = 1;
                nextState = simonPlay;
            end
            end
   
            
    
        simonPlay: begin
            if (lcd_available) begin
                lcd_print = 1;
                topline  =   "  SIMON'S TURN  ";
                bottomline = {"SCORE: ", 4'b0011, TENS, 4'b0011, ONES, "       "};
            end
            tone = randomOutput;
            speakerEn = 1;
            ledEn = 1;
            timerEn = 1;
            color = randomOutput;
            if (timer == 0) begin
                nextState = simonRest;
                timerResetVal = 0.25*oneSecond;
                timerReset = 1;
            end
            end
            
        simonRest: begin
            timerEn = 1;
            if (timer == 0)
                nextState = simonCmp;
        end
        
        simonCmp: begin
            if (simonCount == score) begin
                simonCountReset = 1;
                nextState = playerInit;
            end
            else begin
                simonCountEnable = 1;
                step = 1;
                timerReset = 1;
                timerResetVal = 0.75*oneSecond;
                nextState = simonPlay;
            end
        end
        
        playerInit: begin
            if (lcd_available) begin
                lcd_print = 1;
                topline  =   "   YOUR TURN!   ";
                bottomline = {"SCORE: ", 4'b0011, TENS, 4'b0011, ONES, "       "};
            end
            rerun = 1;
            simonCountReset = 1;
            nextState = playerWaitForPress;
        end
        
        playerWaitForPress: begin
            if (p0 | p1 | p2 | p3) begin
                buttonSave = decodedButton;
                nextState = playerWaitForRelease;
            end
            else if (p4) begin // if the cheat button is pressed
                rerun = 1;
                simonCountReset = 1;
                timerResetVal = oneSecond;
                timerReset = 1;
                nextState = simonPrint;
            
            end
        end
        
        playerWaitForRelease: begin
            tone = buttonSave;
            speakerEn = 1;
            ledEn = 1;
            color = buttonSave;
            if (r0 | r1 | r2 | r3)
                nextState = playerCheck;
        end
        
        playerCheck: begin
            if (buttonSave == randomOutput) begin
                step = 1;
                simonCountEnable = 1;
                if (score == simonCount) begin
                   nextState = wincrement;
                end
                else nextState = playerWaitForPress;
            end
            else begin
                nextState = losecrement;
            end
        end
        
        wincrement: begin
            rerun = 1;
            scoreEn = 1;
            simonCountReset = 1;
            timerResetVal = 0.5*oneSecond;
            timerReset = 1;
            nextState = winTone;
        end
        
        winTone: begin
            // what the heck
            timerEn = 1; // start the timer]
            tone = 5; // set the first tone
            speakerEn = 1;
            if (timer == 0) begin
                timerResetVal = 0.5*oneSecond;
                timerReset = 1;
                nextState = winTone1;
            end
        end
        
        winTone1: begin
            timerEn = 1; // start the timer]
            tone = 6; // set the first tone
            speakerEn = 1;
            if (timer == 0) begin
                timerResetVal = 0.5*oneSecond;
                timerReset = 1;
                nextState = winTone2;
            end
        end
        
        winTone2: begin
            timerEn = 1; // start the timer]
            tone = 7; // set the first tone
            speakerEn = 1;
            if (timer == 0) begin
                timerResetVal = 0.5*oneSecond;
                timerReset = 1;
                nextState = simonPrint;
            end
        
        end
        
        // the only thing about wincrement and losecrement is it makes me think of excrement - ethan, dec 3rd, 2019
        
        losecrement: begin
            rerun = 1;
            scoreReset = 1;
            simonCountReset = 1;
            if (lcd_available) begin
                lcd_print = 1;
                topline  =   "      LOSER     ";
                bottomline = "    NOT SPICY   ";
            end
            nextState = loseTone;
        end
        
        loseTone: begin
            timerEn = 1;
            tone = 7;
            speakerEn = 1;       
            if (timer == 0)
                nextState = loseTone1;
        end
        
         loseTone1: begin
            timerEn = 1; // start the timer]
            tone = 6; // set the first tone
            speakerEn = 1;
            if (timer == 0) begin
                timerResetVal = 0.5*oneSecond;
                timerReset = 1;
                nextState = loseTone2;
            end
        end
        
        loseTone2: begin
            timerEn = 1; // start the timer
            tone = 5; // set the first tone
            speakerEn = 1;
            if (timer == 0) begin
                timerResetVal = 0.5*oneSecond;
                timerReset = 1;
                nextState = playAgain;
            end
        end

        playAgain: begin
        if (lcd_available) begin
                lcd_print = 1;
                topline  =   "  PRESS RED TO  ";
                bottomline = "   PLAY AGAIN   ";
            end
        if (p1) nextState = randomize;
        end
    endcase
end
endmodule